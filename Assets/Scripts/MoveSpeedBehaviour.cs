﻿using Unity.Entities;
using UnityEngine;
using Unity.Burst;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;
using Unity.Collections;
using System.Collections;
using System.Collections.Generic;
using System;


[Serializable]
public struct MoveSpeed : IComponentData
{
    public float Speed;
	public float Gravity;
}

// ReSharper disable once InconsistentNaming
[RequiresEntityConversion]
public class MoveSpeedBehaviour : MonoBehaviour, IConvertGameObjectToEntity
{
	public UIBehaviour ui;
    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
		ui.ball = entity;
		var data = new MoveSpeed() { Speed = 0f,Gravity = 0f};
		dstManager.AddComponentData(entity, data);	
    }
}

[UpdateBefore(typeof(CollisionEventImpulseSystem))]
[UpdateInGroup(typeof(SimulationSystemGroup))]
public class MoveSpeedSystem : JobComponentSystem
{
	private EntityQuery m_Group;
	protected override void OnCreate()
	{
		m_Group = GetEntityQuery(typeof(MoveSpeed), ComponentType.ReadOnly<Translation>());
	}
   
    [BurstCompile]
    struct MoveSpeedJob : IJobForEachWithEntity<Translation,MoveSpeed,PhysicsVelocity>
    {
        public float DeltaTime;
		public float2 PosTap;
		public bool IsTapped;
		public float2 ScreenPoint;
		
        public void Execute(Entity entity, int index,[ReadOnly] ref Translation translation,[ReadOnly] ref MoveSpeed moveSpeed,[WriteOnly] ref PhysicsVelocity velocity)
        {
			//zero speed - instant to no move also
			if(moveSpeed.Speed == 0f)
			{
				velocity.Linear = new float3(0f,0f,0f);
				velocity.Angular = new float3(0f,0f,0f);
				return;
			}
			
			//leaning to a point clicked (tapped)
            if (IsTapped)
            {
                float3 diff = new float3(
                    PosTap.x - ScreenPoint.x,
                    PosTap.y - ScreenPoint.y,
                    0f
                );
				diff = math.normalize(diff);
				float3 sp = new float3(
                    diff.x* moveSpeed.Speed * DeltaTime,
                    0f,
                    0f
                );
				
				velocity.Linear += sp;
				velocity.Angular = new float3(0f,0f,0f);
				return;
            }
			
			//gravitation
			velocity.Linear -= new float3(0f,moveSpeed.Gravity * DeltaTime,0f);
        }
    }
	
    protected override JobHandle OnUpdate(JobHandle inputDependencies)
    {
		//collecting job data tap position 
		Vector3 sP = Vector3.zero;
		var positions = m_Group.ToComponentDataArray<Translation>(Allocator.TempJob);
		for(int i=0;i<positions.Length;i++)
		{
			sP = Camera.main.WorldToScreenPoint(positions[i].Value);
		}
		positions.Dispose();
        var job = new MoveSpeedJob
        {
            DeltaTime = Time.deltaTime,
#if UNITY_EDITOR
			IsTapped = Input.GetMouseButton(0),
			PosTap = new float2(Input.mousePosition.x, Input.mousePosition.y),
#else
			IsTapped = Input.touchCount > 0,
			PosTap = new float2(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y),
#endif
			ScreenPoint =  new float2(sP.x,sP.y)
        }.Schedule(this, inputDependencies); 

        return job; 
    }
}
