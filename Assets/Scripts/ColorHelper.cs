using System;
using UnityEngine;

public static class ColorHelper
{
	public static Color GetRndColor()
	{
		var rndRed = UnityEngine.Random.Range(0f,1f);
		var rndGreen = UnityEngine.Random.Range(0f,1f);
		var rndBlue = UnityEngine.Random.Range(0f,1f);
		return new Color(rndRed,rndGreen,rndBlue);
	}	
}
