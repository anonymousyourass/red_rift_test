using System;
using UnityEngine;

public static class PlayerPrefHelper
{
	private const string ballHitPP = "Ballhit";
	
	private static void setBallHit(int val)
	{
		PlayerPrefs.SetInt(ballHitPP, val);
	}
	
	public static void IncBallHit()
	{
		var m_Ballhit = PlayerPrefHelper.GetBallHit();
		PlayerPrefHelper.setBallHit(++m_Ballhit);
	}
	
	public static int GetBallHit()
	{
		return PlayerPrefs.GetInt(ballHitPP, 0);
	}
}