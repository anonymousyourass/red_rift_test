﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="new planet", menuName="planet")]
public class Planet : ScriptableObject
{
    public float G;
    public Color skyColor;
}
