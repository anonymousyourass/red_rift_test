﻿using Unity.Burst;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Collections;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using Unity.Physics.Authoring;
using UnityEngine;
using Unity.Rendering;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Entities;

public unsafe class UIBehaviour : MonoBehaviour
{
	public Text ballHitTxt;
	
	public Button btnPlanetEarth;
	public Button btnPlanetMoon;
	public Button btnPlanetJupiter;
			
	public Planet Earth;
	public Planet Moon;
	public Planet Jupiter;
	
	public GameObject mainScreen;
	
	public Entity ball;
	public float Speed;

	private const string PRE_BALL_HITS =  "BallHit :";
	
	//raycast
    protected RaycastInput RaycastInput;
    protected NativeList<Unity.Physics.RaycastHit> RaycastHits;
	protected BuildPhysicsWorld m_BuildPhysicsWorldSystem; 
	private EntityManager EntityManager;
	
	 [BurstCompile]
     public struct RaycastJob : IJob
     {
         public RaycastInput RaycastInput;
         public NativeList<Unity.Physics.RaycastHit> RaycastHits;
         [ReadOnly] public PhysicsWorld World;

         public void Execute()
         {
             if (World.CastRay(RaycastInput, out Unity.Physics.RaycastHit hit))
             {
                 RaycastHits.Add(hit);
             }
         }
     }
		
	void Start()
	{
		//callbacks
		Button btn1 = btnPlanetEarth.GetComponent<Button>();
		btn1.onClick.AddListener(OnClickEarth);
		
		Button btn2 = btnPlanetMoon.GetComponent<Button>();
		btn2.onClick.AddListener(OnClickMoon);
		
		Button btn3 = btnPlanetJupiter.GetComponent<Button>();
		btn3.onClick.AddListener(OnClickJupiter);
		//for raycasts
		m_BuildPhysicsWorldSystem = World.Active.GetOrCreateSystem<BuildPhysicsWorld>();
		EntityManager = World.Active.EntityManager;
		RaycastHits = new NativeList<Unity.Physics.RaycastHit>(Allocator.Persistent);
	}
		
	void OnDestroy()
	{
		RaycastHits.Dispose();
	}		
	
	void runQueries(Vector3 orig,Vector3 dir)
    {	
        float3 origin = orig;
        float3 direction = dir;

        RaycastHits.Clear();

        RaycastInput = new RaycastInput
        {
            Start = origin,
            End = origin + direction,
            Filter = CollisionFilter.Default
        };

        new RaycastJob
        {
            RaycastInput = RaycastInput,
            RaycastHits = RaycastHits,
            World = m_BuildPhysicsWorldSystem.PhysicsWorld,
        }.Schedule().Complete();
        
    }
	
	void showQueries()
	{
		if (RaycastHits.IsCreated)
        {
            foreach (Unity.Physics.RaycastHit hit in RaycastHits.ToArray())
            {
				Entity entityHitted = m_BuildPhysicsWorldSystem.PhysicsWorld.Bodies[hit.RigidBodyIndex].Entity;
				if(entityHitted!=ball)
				{
					var renderMesh = EntityManager.GetSharedComponentData<RenderMesh>(entityHitted);
					var rndColor = ColorHelper.GetRndColor();
					renderMesh.material.SetColor("_EmisColor",rndColor);
					EntityManager.SetSharedComponentData<RenderMesh>(entityHitted,renderMesh);	
				}
            }
        }
	}
	
    void Update()
    {
		var hits = PlayerPrefHelper.GetBallHit();
		ballHitTxt.text = PRE_BALL_HITS + hits;
		
		//back button
		if (Input.GetKeyDown(KeyCode.Escape)) 
		{ 
			mainScreen.SetActive(true);
			applyMenu(); 
		}
		//raycast to repaint platforms
#if UNITY_EDITOR
		if(Input.GetMouseButton(0))
#else
		if(Input.touchCount == 1)
#endif
		{
			UnityEngine.RaycastHit hit;
#if UNITY_EDITOR
			UnityEngine.Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
			runQueries(ray.origin,ray.direction);
#else
			UnityEngine.Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, 0));
			runQueries(ray.origin,ray.direction);
#endif

			showQueries();
		}
    }
	
	private void OnClickEarth()
	{
		mainScreen.SetActive(false);
		applyPlanet(Earth);
	}

	
	private void OnClickMoon()
	{
		mainScreen.SetActive(false);
		applyPlanet(Moon);
	}
	
	private void OnClickJupiter()
	{
		mainScreen.SetActive(false);
		applyPlanet(Jupiter);
	}
	
	public void applyPlanet(Planet planet)
	{
		var data = new MoveSpeed() { Speed = this.Speed,Gravity = planet.G};
		EntityManager.SetComponentData(ball, data);	
	}
	
	public void applyMenu()
	{
		var data = new MoveSpeed() { Speed = 0f,Gravity = 0f};
		EntityManager.SetComponentData(ball, data);	
	}
}