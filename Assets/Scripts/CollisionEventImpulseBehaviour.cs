﻿using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using UnityEngine;
using Collider = Unity.Physics.Collider;
using Unity.Transforms;
using Unity.Profiling;
using Unity.Burst;
using System;
using UnityEditor;
using Unity.Mathematics;
using System.Collections.Generic;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Rendering;

public struct CollisionEventImpulse : IComponentData
{
    public float3 Impulse;
}

public class CollisionEventImpulseBehaviour : MonoBehaviour, IConvertGameObjectToEntity
{
    public float Magnitude = 1.0f;

    void OnEnable() { }

    void IConvertGameObjectToEntity.Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        if (enabled)
        {
            dstManager.AddComponentData(entity, new CollisionEventImpulse()
            {
                Impulse = Magnitude,
            });
        }
    }
}

// A Repulsor is defined by a PhysicsShape with the `Raise Collision Events` flag ticked and a
// CollisionEventImpulse behaviour added.
[UpdateAfter(typeof(EndFramePhysicsSystem))]
unsafe public class CollisionEventImpulseSystem : JobComponentSystem
{
    BuildPhysicsWorld m_BuildPhysicsWorldSystem;
    StepPhysicsWorld m_StepPhysicsWorldSystem;
	
	private EntityQuery render_Group;
	private EntityManager EntityManager;
	
    protected override void OnCreate()
    {
        m_BuildPhysicsWorldSystem = World.GetOrCreateSystem<BuildPhysicsWorld>();
        m_StepPhysicsWorldSystem = World.GetOrCreateSystem<StepPhysicsWorld>();

		var query = new EntityQueryDesc
		{
			None = new ComponentType[]{ typeof(MoveSpeed)},
			All = new ComponentType[]{ typeof(RenderMesh),typeof(CollisionEventImpulse)}, 
		};
		
		render_Group = GetEntityQuery(query);
   
		EntityManager = World.Active.EntityManager;
    }


    [BurstCompile]
    struct CollisionEventImpulseJob : ICollisionEventsJob
    {
        [ReadOnly] public ComponentDataFromEntity<CollisionEventImpulse> ColliderEventImpulseGroup;
        public ComponentDataFromEntity<PhysicsVelocity> PhysicsVelocityGroup;
		public NativeArray<bool> resultCollision;
		public NativeArray<int> resultBodyIdx;
		
        public void Execute(CollisionEvent collisionEvent)
        {
            Entity entityA = collisionEvent.Entities.EntityA;
            Entity entityB = collisionEvent.Entities.EntityB;

            bool isBodyADynamic = PhysicsVelocityGroup.Exists(entityA);
            bool isBodyBDynamic = PhysicsVelocityGroup.Exists(entityB);

            bool isBodyARepulser = ColliderEventImpulseGroup.Exists(entityA);
            bool isBodyBRepulser = ColliderEventImpulseGroup.Exists(entityB);

            if(isBodyARepulser && isBodyBDynamic)
            {
                var impulseComponent = ColliderEventImpulseGroup[entityA];
                var velocityComponent = PhysicsVelocityGroup[entityB];
                
                velocityComponent.Linear = math.length(impulseComponent.Impulse.xyz)*collisionEvent.Normal;
                PhysicsVelocityGroup[entityB] = velocityComponent;
				resultCollision[0] = true;
				resultBodyIdx[0] = collisionEvent.BodyIndices.BodyAIndex;
            }
            else if (isBodyBRepulser && isBodyADynamic)
            {
                var impulseComponent = ColliderEventImpulseGroup[entityB];
                var velocityComponent = PhysicsVelocityGroup[entityA];
                velocityComponent.Linear = math.length(impulseComponent.Impulse.xyz) * collisionEvent.Normal;
                PhysicsVelocityGroup[entityA] = velocityComponent;
				resultCollision[0] = true;
				resultBodyIdx[0] = collisionEvent.BodyIndices.BodyBIndex;
            }
        }
    }
	
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
		NativeArray<bool> result = new NativeArray<bool>(1, Allocator.TempJob);
		NativeArray<int> resultIdx = new NativeArray<int>(1, Allocator.TempJob);
		
		var job = new CollisionEventImpulseJob
        {
            ColliderEventImpulseGroup = GetComponentDataFromEntity<CollisionEventImpulse>(true),
            PhysicsVelocityGroup = GetComponentDataFromEntity<PhysicsVelocity>(),
        };
		
		job.resultCollision = result;
		job.resultBodyIdx = resultIdx;
		
        var jobHandle = job.Schedule(m_StepPhysicsWorldSystem.Simulation, 
                    ref m_BuildPhysicsWorldSystem.PhysicsWorld, inputDeps);

		jobHandle.Complete();
		
		bool IsCollided = result[0];
		if( IsCollided)
		{
			//increase collided counter to display in UI
			PlayerPrefHelper.IncBallHit();	
			//re - paint collided entity RenderMesh'es Material color
			int collidedBodyIdx = resultIdx[0];
			Entity entityCollided = m_BuildPhysicsWorldSystem.PhysicsWorld.Bodies[collidedBodyIdx].Entity;
			var renderMesh = EntityManager.GetSharedComponentData<RenderMesh>(entityCollided);
			var rndColor = ColorHelper.GetRndColor();
			renderMesh.material.SetColor("_EmisColor",rndColor);
			EntityManager.SetSharedComponentData<RenderMesh>(entityCollided,renderMesh);
		}	

		result.Dispose();
		resultIdx.Dispose();
		
        return jobHandle;
    }
}